import React from 'react';
import Auxi from '../../hoc/Auxi';
import classes from './Layout.module.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

class Layout extends React.Component {
  state={
    showSideDrawer : false
  }
  sideDrawerClosedHandler = () => {
    this.setState({
      showSideDrawer : false
    });
  }
  sideDrawerToggleHandler = () => {
    this.setState((prevState) => {
      return { showSideDrawer : !prevState.showSideDrawer};
    });
  }
  render(){
    return(
      <Auxi>
        <Toolbar opened={this.sideDrawerToggleHandler} />
        <SideDrawer open={this.state.showSideDrawer} closed={this.sideDrawerClosedHandler}/>
        <main className={classes.content}>
          {this.props.children}
        </main>
      </Auxi>
    );
  }
}

export default Layout;
