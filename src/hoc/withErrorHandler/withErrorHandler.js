import React from 'react';

import Modal from '../../components/UI/Modal/Modal';
import Aux from '../Auxi';

const ErrorHandler = (WrappedComponent) => {
  return (props) => (
    <Aux>
      <Modal show>
        Something didn't work
      </Modal>
      <WrappedComponent {...props} />
    </Aux>
  );
}

export default ErrorHandler;
