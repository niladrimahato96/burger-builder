import axios from 'axios';

const instance = axios.create({
  baseURL : 'https://react-my-burger-929bc.firebaseio.com/'
});

export default instance;
